<?php
class Search {
    /**
     * @var object $db the database connection object
     */
    private $db;
    public function __construct(PDO $db) {
        $this->db = $db;
    }
    /**
     * Search the posts for a search term
     * 
     * @param string $query The search query
     * @return array search results
     * @throws PDOException
     */
    public function look_for ($query = '') {
        try {
            $sql = "select * from advertisement where title like :query or description like :query";
            $conn = $this->db->prepare($sql);
            $conn->bindValue(":query", "%$query%", PDO::PARAM_STR);
            $conn->execute();
            // $conn->debugDumpParams();
            return $conn->fetchAll();
        } catch (PDOException $e) {
            throw $e;
        }
    }
}