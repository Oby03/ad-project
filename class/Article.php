<?php

class Article {
  private $db;

  public function __construct(PDO $db) {
    $this->db = $db;
  }

  public function create($body, $author, $status, $path, $type) {
    $sql = "INSERT INTO feeds (body, timePosted, status, author, path, type)
            VALUES (:body, NOW(), :status, :author, :path, :type)";

    $conn = $this->db->prepare($sql);
    $conn->bindValue(":body",$body, PDO::PARAM_STR);
    $conn->bindValue(":author",$author, PDO::PARAM_STR);
    $conn->bindValue(":status", $status);
    $conn->bindValue(":path", $path);
    $conn->bindValue(":type", $type);

    return $conn->execute();
  }

  public function read($status) {
    $sql = "SELECT * FROM feeds WHERE status = :status ORDER BY id DESC LIMIT 30";
    $conn = $this->db->prepare($sql);
    $conn->bindValue(":status", $status, PDO::PARAM_INT);

    try {
      $conn->execute();
      $result = $conn->fetchAll();
      return $result;

    } catch (PDOException $e) {
      $e->getMessage();
    }

  }

  public function readOne($id) {
    $sql = "SELECT * FROM feeds WHERE id = $id";
    $conn = $this->db->prepare($sql);

    try {
      $conn->execute();
      $result = $conn->fetchAll();
      return $result;
    } catch (PDOException $e) {
      $e->getMessage();
    }

  }

  public function update($id, $body) {
    try {
    $sql = "UPDATE feeds SET body  = :body WHERE id = :id";
    $conn = $this->db->prepare($sql);

    $conn->bindValue(":id", $id, PDO::PARAM_INT);
    $conn->bindValue(":body", $body, PDO::PARAM_STR);

    $result = $conn->execute();
    return  $result;

    }  catch (PDOException $e) {
      $e->getMessage();
    }

  }

  /* Delete Posts and all their related comments */
  public function delete($id, $status) {
    try {
    $sql = "UPDATE feeds SET status = :status WHERE id = :id";
    $conn = $this->db->prepare($sql);

    $conn->bindValue(":id", $id, PDO::PARAM_INT);
    $conn->bindValue(":status", $status, PDO::PARAM_INT);

    $result = $conn->execute();
    return $result;

    }  catch(PDOException $e) {
      $e->getMessage();
    }

  }

  public function like($id) {
    try {
      $sql = "UPDATE feeds SET votes= :status WHERE id = :id";
      $conn = $this->db->prepare($sql);

      $coon->bindValue(":id", $id, PDO::PARAM_INT);

      $result = $conn->execute();
      return $result; 
    } catch (PDOException $e) {
      $e->getMessage();
    }
  }

} /* ending class Blog */

// include 'config.php';
// $article = new Article($db);
//
// #$article->create("Article 1", "mark", 1);
// echo "Happening\n";
// #var_dump($article->read(1));
// var_dump($article->create("Another article here", "mark", 1));
// #var_dump($article->readOne(1));
//
?>
