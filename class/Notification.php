<?php

// include('../models/config.php');

class Notification {

    private $db;

    public function __construct(PDO $db)
    {
        $this->db = $db;
    }

    public function setNotification($user_to, $notification_text, $seen)
    {
        $sql = "INSERT INTO notification (user_to, notification_text, seen  ) VALUES (:user_to, :notification_text, :seen)";

        $conn = $this->db->prepare($sql);
        $conn->bindValue(':user_to', $user_to, PDO::PARAM_INT);
        $conn->bindValue(':notification_text', $notification_text, PDO::PARAM_STR);
        $conn->bindValue(':seen', $seen, PDO::PARAM_INT);

        try
        {
            $result = $conn->execute();
            return $result;
        }
        catch (PDOException $e)
        {
            echo $e->getMessage();
        }
    }

    public function setSeen($notification_id)
    {
        $sql = "UPDATE notification SET seen = 1 WHERE id = :notification_id";

        $conn = $this->db->prepare($sql);
        $conn->bindValue(':notification_id', $notification_id, PDO::PARAM_STR);

        try
        {
            $result = $conn->execute();
            return $result;
        }
        catch (PDOException $e)
        {
            echo $e->getMessage();
        }
    }

    public function hasNotifications($user_id)
    {
        $sql = "SELECT * FROM notification WHERE seen  = 0 AND user_to = :user_id";

        $conn = $this->db->prepare($sql);
        $conn->bindValue(':user_id', $user_id, PDO::PARAM_INT);

        try
        {
            $conn->execute();
            $result = $conn->fetchColumn();
            return $result;
        }
        catch (PDOException $e)
        {
            echo $e->getMessage();
        }
    }
}


// $notification = new Notification($db);


// // $notification->setNotification(2, 'You have a new message', 0);


// $notification->setSeen(1);


// $notif = $notification->hasNotifications(1);

// var_dump($notif);
