<?php class Likes {
	private $db;

    public function __construct(PDO $conn) 
    {
        $this->db = $conn;
    }

    public function like ($adv_id, $user_id)
    {
    	$ids = $this->get_likers($adv_id);
    	$likers = explode(',', $ids);
    	if (
    		!in_array($user_id, $likers)
    	) {
    		// this means i've never liked before so i'll like
    		array_push($likers, $user_id);
    		$sql = "UPDATE advertisement SET like_ids = :ids, votes = votes + 1 WHERE id=:adv_id";
    		$conn = $this->db->prepare($sql);
        	$conn->bindValue(':adv_id', $adv_id, PDO::PARAM_STR);
        	$conn->bindValue(':ids', implode(',', $likers), PDO::PARAM_STR);
        	$conn->execute();
        	return $this->get_likes($adv_id);
    	} else {
    		// this means i've liked before so i'll unlike
    		$newlikers = array_diff($likers, ["$user_id"]);
    		$sql = "UPDATE advertisement SET like_ids = :ids, votes = votes - 1 WHERE id=:adv_id";
    		$conn = $this->db->prepare($sql);
        	$conn->bindValue(':adv_id', $adv_id, PDO::PARAM_STR);
        	$conn->bindValue(':ids', implode(',', $newlikers), PDO::PARAM_INT);
        	$conn->execute();
        	return $this->get_likes($adv_id);
    	}
    }

    public function unlike ($adv_id, $user_id)
    {
    	$likers = explode(',', $this->get_likers($adv_id));
    	if (
    		in_array($user_id, $likers)
    	) {
    		// only unlike the photos you've liked
    		$likers = array_diff($likers, ["$user_id"]);
    		$sql = "UPDATE advertisement SET like_ids = :ids, votes = votes + 1 WHERE id=:adv_id";
    		$conn = $this->db->prepare($sql);
        	$conn->bindValue(':adv_id', $adv_id, PDO::PARAM_STR);
        	$conn->bindValue(':ids', implode(',', $likers), PDO::PARAM_INT);
        	$conn->execute();
        	return $this->get_likes();
    	}

    	return false;
    }

    public function get_likers ($adv_id)
    {
    	$sql = "SELECT like_ids FROM advertisement WHERE id = :id LIMIT 1";
        
        $conn = $this->db->prepare($sql);
        $conn->bindValue(':id', $adv_id, PDO::PARAM_INT);

        try {
            $conn->execute();
            $data = $conn->fetch();
            return $data['like_ids'];

        } catch (PDOException $e) {
            throw $e;
        }
    }

    public function get_likes ($adv_id)
    {
    	$sql = "SELECT votes FROM advertisement WHERE id = :id LIMIT 1";
        
        $conn = $this->db->prepare($sql);
        $conn->bindValue(':id', $adv_id, PDO::PARAM_INT);

        try {
            $conn->execute();
            $data = $conn->fetch();
            return $data['votes'];

        } catch (PDOException $e) {
            throw $e;
        }
    }
}