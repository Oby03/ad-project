<?php

	define("DB_DSN","mysql:host=localhost;dbname=admgt");
	define("DB_USR","root");
	define("DB_PWD","");
	
	try {
		$db = new PDO(DB_DSN, DB_USR, DB_PWD);
		$db->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
	}  catch (PDOException $e) {
		throw $e;
	}

?>
