$(function () {
	$('.likeAd').on("click", function () {
		//
		var $id = $(this).data('id');
		var $user_id = $(this).data('liker');
		if ($id) {
			$.ajax({
		          url: "/like",
		          type: "POST",
		          dataType: "JSON",
		          data: { 
		            post_id: $id,
		            user_id: $user_id
		        },
		        // handle a successful response
		        success: function(data) {
		            $(this).find('span').html(data.likes);
		        }.bind(this),
		        error: function(xhr, errmsg, err) {
		            $.growl.error({ message: "Error liking advert" });         
		        }.bind(this),
	    	});
		}
		//
	});
})